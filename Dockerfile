FROM docker.io/library/alpine:3.17

# hadolint ignore=DL3004,DL3013,DL3018,DL4006
RUN set -eux \
  ; apk add --quiet --update --no-cache \
        bind-tools \
  ; rm -rf /var/lib/apk/* /var/cache/apk/* \
  ; dig -v

USER 65534:65534
