{{/*
Expand the name of the chart.
*/}}
{{- define "nodelocaldns.name" -}}
{{-   default .Chart.Name .Values.nameOverride | trunc 63 | trimSuffix "-" -}}
{{- end -}}

{{/*
Create a default fully qualified app name.
We truncate at 63 chars because some Kubernetes name fields are limited to this (by the DNS naming spec).
If release name contains chart name it will be used as a full name.
*/}}
{{- define "nodelocaldns.fullname" -}}
{{-   if .Values.fullnameOverride -}}
{{-     .Values.fullnameOverride | trunc 63 | trimSuffix "-" -}}
{{-   else -}}
{{-     $name := default .Chart.Name .Values.nameOverride -}}
{{-     if contains $name .Release.Name -}}
{{-       .Release.Name | trunc 63 | trimSuffix "-" -}}
{{-     else -}}
{{-       printf "%s-%s" .Release.Name $name | trunc 63 | trimSuffix "-" -}}
{{-     end -}}
{{-   end -}}
{{- end -}}

{{/*
Create a upstream service name.
*/}}
{{- define "nodelocaldns.upstreamServiceName" -}}
{{-   printf "%s-upstream" (include "nodelocaldns.fullname" .) | trunc 63 | trimSuffix "-" -}}
{{- end -}}

{{/*
Create a metrics service name.
*/}}
{{- define "nodelocaldns.metricsServiceName" -}}
{{-   printf "%s-metrics" (include "nodelocaldns.fullname" .) | trunc 63 | trimSuffix "-" -}}
{{- end -}}

{{/*
Create chart name and version as used by the chart label.
*/}}
{{- define "nodelocaldns.chart" -}}
{{-   printf "%s-%s" .Chart.Name .Chart.Version | replace "+" "_" | trunc 63 | trimSuffix "-" -}}
{{- end -}}

{{/*
Ensure clusterDns is set.
*/}}
{{- define "nodelocaldns.checkClusterDns" -}}
{{-   required "Value for `clusterDns` must be set!" .Values.clusterDns -}}
{{- end -}}

{{/*
Ensure logQueries is set properly.
*/}}
{{- define "nodelocaldns.checkLogQueries" -}}
{{-   $values := list "none" "all" "success" "denial" -}}
{{-   if not (has .Values.logQueries $values) -}}
{{-     fail "Incorrect value for `logQueries`" -}}
{{-   end -}}
{{- end -}}

{{/*
Check values.
*/}}
{{- define "nodelocaldns.checkValues" -}}
{{-   include "nodelocaldns.checkClusterDns" . -}}
{{-   include "nodelocaldns.checkLogQueries" . -}}
{{- end -}}

{{/*
Print map of string (labels, annotations)
*/}}
{{- define "nodelocaldns.printMap" -}}
{{-   $map := . -}}
{{-   $keys := keys . | sortAlpha -}}
{{-   range $i, $key := $keys -}}
{{-     $value := get $map $key | toString | trimSuffix "\n" | trim -}}
{{-     printf "%s: %s\n" $key (quote $value) -}}
{{-   end -}}
{{- end -}}

{{/*
Minimal set of labels
*/}}
{{- define "nodelocaldns.minimalLabels" -}}
{{-   $labels := .Values.labels -}}
{{-   $labels := set $labels "helm.sh/chart" (include "nodelocaldns.chart" .) -}}
{{-   $labels := set $labels "app.kubernetes.io/managed-by" .Release.Service -}}
{{-   include "nodelocaldns.printMap" $labels -}}
{{- end -}}

{{/*
Selector labels
*/}}
{{- define "nodelocaldns.selectorLabels" -}}
{{-   $labels := dict -}}
{{-   $labels := set $labels "app.kubernetes.io/name" (include "nodelocaldns.name" .) -}}
{{-   $labels := set $labels "app.kubernetes.io/instance" .Release.Name -}}
{{-   include "nodelocaldns.printMap" $labels -}}
{{- end -}}

{{/*
Common labels
*/}}
{{- define "nodelocaldns.labels" -}}
{{-   $labels := dict -}}
{{-   $labels := include "nodelocaldns.selectorLabels" . | fromYaml | mergeOverwrite $labels -}}
{{-   $labels := include "nodelocaldns.minimalLabels" . | fromYaml | mergeOverwrite $labels -}}
{{-   $labels := set $labels "app.kubernetes.io/version" .Chart.AppVersion -}}
{{-   include "nodelocaldns.printMap" $labels -}}
{{- end -}}

{{/*
Pod labels
*/}}
{{- define "nodelocaldns.podLabels" -}}
{{-   $labels := .Values.podLabels -}}
{{-   $labels := include "nodelocaldns.labels" . | fromYaml | mergeOverwrite $labels -}}
{{-   $labels := include "nodelocaldns.minimalLabels" . | fromYaml | mergeOverwrite $labels -}}
{{-   include "nodelocaldns.printMap" $labels -}}
{{- end -}}

{{/*
Create the name of the service account to use
*/}}
{{- define "nodelocaldns.serviceAccountName" -}}
{{-   if .Values.serviceAccount.create -}}
{{-     default (include "nodelocaldns.fullname" .) .Values.serviceAccount.name -}}
{{-   else -}}
{{-     default "default" .Values.serviceAccount.name -}}
{{-   end -}}
{{- end -}}
