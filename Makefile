SHELL = /usr/bin/env bash -o pipefail
.SHELLFLAGS = -ec

CHART_VERSION = $(shell grep '^version:' Chart.yaml | grep -oE '[0-9]+.[0-9]+.[0-9]+')
COMMIT_SHA = $(shell git rev-parse HEAD)
IMAGE_TAG_BASE ?= registry.gitlab.com/x-ops/helm/nodelocaldns/dnstools
IMAGE_TAG ?= $(IMAGE_TAG_BASE):$(COMMIT_SHA)

#
# --- Generators
#

.PHONY: schema-gen
schema-gen:
	@helm schema-gen values.yaml > values.schema.json

.PHONY: docs
docs:
	@helm-docs .

#
# -- Linters
#

.PHONY: markdownlint
markdownlint: docs
	@markdownlint .

.PHONY: yamllint
yamllint:
	@yamllint .

.PHONY: hadolint
hadolint:
	@hadolint Dockerfile

.PHONY: helm-lint
helm-lint:
	@helm lint . --strict --set-string clusterDns=10.20.30.40

.PHONY: lint
lint: markdownlint yamllint hadolint helm-lint

#
# --- Builders
#

.PHONY: package
package: clean
	@helm package .

.PHONY: image
image:
	@docker build . --tag $(IMAGE_TAG)

.PHONY: publish-image
publish-image: image
	@docker push $(IMAGE_TAG)
	@sed -i values.yaml -r -e 's_^testHookImage:.+_testHookImage: $(IMAGE_TAG)_g'

#
# --- Tools
#

.PHONY: info
info:
	@echo "Chart version: $(CHART_VERSION)"
	@echo "Commit SHA: $(COMMIT_SHA)"
	@echo "Image tag: $(IMAGE_TAG)"

.PHONY: template
template:
	@helm template . --set-string clusterDns=10.20.30.40 --debug

.PHONY: pre-commit-autoupdate
pre-commit-autoupdate:
	@pre-commit autoupdate

.PHONY: pre-commit-install
pre-commit-install:
	@pre-commit install

.PHONY: clean
clean:
	@rm -f *.tgz *.tar.gz

.PHONY: release
release: clean lint package
	@git add --all
	@git commit -m "Release $(CHART_VERSION)"
	@git tag "v$(CHART_VERSION)"
