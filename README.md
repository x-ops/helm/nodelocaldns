# nodelocaldns

![Version: 0.1.2](https://img.shields.io/badge/Version-0.1.2-informational?style=flat-square) ![Type: application](https://img.shields.io/badge/Type-application-informational?style=flat-square) ![AppVersion: 1.22.20](https://img.shields.io/badge/AppVersion-1.22.20-informational?style=flat-square)

Nodelocal DNS Cache

**Homepage:** <https://gitlab.com/x-ops/helm/nodelocaldns>

## Maintainers

| Name | Email | Url |
| ---- | ------ | --- |
| Anton Kulikov |  | <https://gitlab.com/4ops> |

## Source Code

* <https://github.com/kubernetes/kubernetes/tree/master/cluster/addons/dns/nodelocaldns>
* <https://gitlab.com/x-ops/helm/nodelocaldns>

## Values

| Key | Type | Default | Description |
|-----|------|---------|-------------|
| clusterDns | string | `""` | IP address of the cluster DNS service. You can get it using command `kubectl -n kube-system get svc kube-dns -o jsonpath --template '{.spec.clusterIP}'`. This value is **required**. |
| clusterDomain | string | `"cluster.local"` | DNS-suffix for kubernetes cluster. Only used if `defaultResolver` is not `cluster`. |
| defaultResolver | string | `"/etc/resolv.conf"` | Resolver for `.` domain. Possible values are: `cluster`, `/etc/resolv.conf` or IP address of the recursive DNS server. |
| extraForwardList | list | `[]` |  |
| extraServerBlocks | string | `""` |  |
| extraVolumeMounts | list | `[]` |  |
| extraVolumes | list | `[]` |  |
| fullnameOverride | string | `""` | Overrides the full name |
| image | object | `{"pullPolicy":"IfNotPresent","repository":"registry.k8s.io/dns/k8s-dns-node-cache","tag":""}` | - Container  |
| image.pullPolicy | string | `"IfNotPresent"` | Image download policy. See: https://kubernetes.io/docs/concepts/containers/images/#updating-images |
| image.repository | string | `"registry.k8s.io/dns/k8s-dns-node-cache"` | Overrides the image repository |
| image.tag | string | Application version from chart's `appVersion` | Overrides the image tag |
| imagePullSecrets | list | `[]` |  |
| invalidDomains.domains | list | `[]` | Ivalid domain names Placeholder `_kubernetes_` will be replaced with cluster DNS suffix such as `cluster.local`. |
| invalidDomains.enabled | bool | `true` |  |
| invalidDomains.namespaces | list | `[]` |  |
| invalidDomains.tld | list | `[]` |  |
| labels | map | map[] | Additional labels for all resources |
| localIpAddress | string | `"169.254.20.10"` |  |
| logQueries | string | `"none"` | Enable logging. Possible values are: `none`, `all`, `success` or `denial`. |
| metrics.enabled | bool | `true` | Enables prometheus metrics |
| metrics.podMonitor.create | bool | `false` |  |
| metrics.podMonitor.jobLabel | string | `""` |  |
| metrics.podMonitor.name | string | `""` |  |
| metrics.podMonitor.namespace | string | `""` |  |
| metrics.podMonitor.scrapeInterval | string | `"30s"` |  |
| metrics.podMonitor.scrapeTimeout | string | `"15s"` |  |
| metrics.port | int | `9253` | Metrics port number |
| metrics.serviceMonitor.create | bool | `false` |  |
| metrics.serviceMonitor.jobLabel | string | `""` |  |
| metrics.serviceMonitor.name | string | `""` |  |
| metrics.serviceMonitor.namespace | string | `""` |  |
| metrics.serviceMonitor.scrapeInterval | string | `"30s"` |  |
| metrics.serviceMonitor.scrapeTimeout | string | `"15s"` |  |
| nameOverride | string | `""` | Overrides the chart name |
| networkPolicy | object | `{"create":false,"monitoringNamespace":"monitoring"}` | - Additional resources  |
| podAnnotations | yaml | map[] | Additional labels for Pods |
| podLabels | yaml | map[] | Additional labels for Pods |
| podSecurityContext | yaml | map[] | Pod security context |
| priorityClassName | string | `"system-node-critical"` | Priority class name |
| resources | object | `{}` |  |
| securityContext | object | `{}` | Overrides default container security context. |
| serviceAccount.annotations | object | `{}` |  |
| serviceAccount.create | bool | `true` |  |
| serviceAccount.name | string | If not set and create is true, a name is generated using the fullname template | The name of the service account to use. |
| testHookArgs[0] | string | `"nslookup"` |  |
| testHookArgs[1] | string | `"-nodebug"` |  |
| testHookArgs[2] | string | `"-querytype=A"` |  |
| testHookArgs[3] | string | `"google.com"` |  |
| testHookImage | string | `"registry.gitlab.com/x-ops/helm/nodelocaldns/dnstools:47c413b4125181797ec6c8b97b024f88cc5d66b8"` | - Test hook  |
| updateStrategy | object | `{"rollingUpdate":{"maxUnavailable":"10%"}}` | - DaemonSet  |

----------------------------------------------
Autogenerated from chart metadata using [helm-docs v1.11.0](https://github.com/norwoodj/helm-docs/releases/v1.11.0)
